import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Network } from '@ionic-native/network';
import { TouchID } from '@ionic-native/touch-id';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth';

import { ListPage } from '../pages/list/list';
import { AddItemPage } from '../pages/add-item/add-item';
import { EditItemPage } from '../pages/edit-item/edit-item';
import { DetailItemPage } from '../pages/detail-item/detail-item';
import { TabsPage } from '../pages/tabs/tabs';
import { ConnectionPage } from '../pages/connection/connection';
import { PlaceholderPage } from '../pages/placeholder/placeholder';

import { ListProvider } from '../providers/list/list';
import { IonicStorageModule } from '@ionic/storage';
import { NavProxyProvider } from '../providers/nav-proxy/nav-proxy';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { HTTP } from '@ionic-native/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { NetworkStatusProvider } from '../providers/network-status/network-status';


@NgModule({
  declarations: [
    MyApp,
    AddItemPage,
    DetailItemPage,
    TabsPage, 
    ListPage,
    ConnectionPage,
    EditItemPage,
    PlaceholderPage,
   
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddItemPage,
    DetailItemPage,
    TabsPage,
    ListPage,
    ConnectionPage,
    EditItemPage,
    PlaceholderPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ListProvider,
    File,
    Camera,
    FilePath,
    NavProxyProvider, 
    HTTP, 
    BarcodeScanner,
    NetworkStatusProvider,
    Network,
    TouchID,
    AndroidFingerprintAuth
  ]
})
export class AppModule {}
