import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';


import { NavProxyProvider } from '../providers/nav-proxy/nav-proxy';
import { PlaceholderPage } from '../pages/placeholder/placeholder';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = TabsPage;
  @ViewChild('detailNav') detailNav: Nav;
  @ViewChild('masterNav') masterNav: Nav;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private NavProxy: NavProxyProvider) {
    platform.ready().then(() => {
      NavProxy.masterNav = this.masterNav;
      NavProxy.detailNav = this.detailNav;
      this.masterNav.setRoot(TabsPage);
      this.detailNav.setRoot(PlaceholderPage);

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
