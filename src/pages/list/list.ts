import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ListProvider } from '../../providers/list/list';
import { DetailItemPage } from '../detail-item/detail-item';
import { ConnectionPage } from '../connection/connection';

import { _MasterPage } from '../';
import { AlertController } from 'ionic-angular';
import { NavProxyProvider } from '../../providers/nav-proxy/nav-proxy';
import { NetworkStatusProvider } from '../../providers/network-status/network-status';
import { ModalController } from 'ionic-angular';


declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage extends _MasterPage {

  //Variable à vraie quand le mode édition est actif
  public editActive = false;
  //Nombre d'items cochés
  public nbChecked = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public ListService: ListProvider, private alertCtrl: AlertController, private NavProxyService: NavProxyProvider, public NetworkStatus : NetworkStatusProvider, public modalCtrl: ModalController, private platform: Platform) {
    super();
    this.getData();
    
  }

  ionViewDidLoad(){
    //S'il n'y a pas de connexion
    if(this.NetworkStatus.Type == "none"){
      this.errorAlert;
    }
    //On affiche l'écran de connexion
    let AddModal = this.modalCtrl.create(ConnectionPage);
    AddModal.present();    
  }

  //Affichage message d'alerte quand il n'y a pas de connexion
  errorAlert(){
    let alert = this.alertCtrl.create({
      title: 'Aucune connexion !',
      message: 'Vous ne pouvez pas scanner d\'articles mais vous pourrez toujours en ajouter manuellement ',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  //On récupère la liste dans le stockage
  getData() {
    this.ListService.getListData().then((data) => {
      if (data) {
        this.ListService.itemList = data;
      }
    });
  }

  //Supprimer un item passé en paramètre
  suppItem(item) {
    const index: number = this.ListService.itemList.indexOf(item);
    if (index !== -1) {
      this.ListService.itemList.splice(index, 1);
    }
    this.ListService.saveListData(this.ListService.itemList);
    this.UpdatenbChecked();
  }

  //Supprimer tout les items de la liste
  suppAllItems() {
    this.ListService.itemList = [];
    this.ListService.saveListData(this.ListService.itemList);
    //On désactive le mode édition
    this.EditActive(false);
  }

  //Supprimer les items sélectionnés
  suppSelectedItems() {
    for (let i = this.ListService.itemList.length - 1; i >= 0; i--) {
      if (this.ListService.itemList[i].checked == true) {
        this.suppItem(this.ListService.itemList[i]);
      }
    }
    this.EditActive(false);
  }

  //Changer l'attribut "checked" de l'item passé en paramètre selon qu'il ai été coché ou décoché
  SelectItem(itemList) {
    if (itemList.checked) {
      itemList.checked = false;
    } else {
      itemList.checked = true;
    }
    this.UpdatenbChecked();
  }

  //On met à jour le nombre d'items cochés
  UpdatenbChecked() {
    //On va mettre à jour nbChecked
    let nb = 0;
    for (let item of this.ListService.itemList) {
      if (item.checked) {
        nb++;
      }
    }
    this.nbChecked = nb;
  }

  //On change le booléen EditActive selon la variable en paramètres qui est à vrai quand le mode édition est activé (pour supprimer des items) 
  EditActive(bool) {
    this.editActive = bool;
    //On réinitialise nbChecked et l'attribut checked des items de itemlist
    this.nbChecked = 0;
    for (let item of this.ListService.itemList) {
      item.checked = false;
    }
  }

  //Affiche un message d'alerte pour la confirmation de la suppression de tout les items
  ConfirmSuppAll() {
    let alert = this.alertCtrl.create({
      title: 'Voulez-vous vraiment supprimer toute la liste ?',
      message: 'Cette action est irréversible',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Annulation');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            this.suppAllItems();
          }
        }
      ]
    });
    alert.present();
  }

  //Affiche un message d'alerte pour la confirmation de la suppression des items sélectionnés
  ConfirmSuppSelected() {
    let alert = this.alertCtrl.create({
      title: 'Voulez-vous vraiment supprimer ces ' + this.nbChecked + ' éléments ?',
      message: 'Cette action est irréversible',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Annulation');
          }
        },
        {
          text: 'Oui',
          handler: () => {
            this.suppSelectedItems();
          }
        }
      ]
    });
    alert.present();
  }

  //On affiche la page du détail de l'item cliqué
  viewItem(item) {
    this.NavProxyService.pushDetail(DetailItemPage, {
      item: item

    });
  }

  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }
}
