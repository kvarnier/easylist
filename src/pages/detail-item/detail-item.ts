import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { EditItemPage } from '../edit-item/edit-item';
import { _DetailPage } from '../';
import { NavProxyProvider } from '../../providers/nav-proxy/nav-proxy';
import { ListProvider } from '../../providers/list/list';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-detail-item',
  templateUrl: 'detail-item.html',
})
export class DetailItemPage extends _DetailPage {

  public item: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public modalCtrl: ModalController, private NavProxyService: NavProxyProvider,public ListService: ListProvider) {
    super();
  }

  ionViewDidLoad() {
    this.item.title = this.navParams.get('item').title;
    this.item.quantity = this.navParams.get('item').quantity;
    this.item.category = this.navParams.get('item').category;
    this.item.image = this.navParams.get('item').image;
    this.item.brand = this.navParams.get('item').brand;
    this.item.weight = this.navParams.get('item').weight;
    this.item.sellingCountry = this.navParams.get('item').sellingCountry;
    this.item.ingredient = this.navParams.get('item').ingredient;
    this.item.packaging = this.navParams.get('item').packaging;
    this.item.imageScan = this.navParams.get('item').imageScan;
    this.viewCtrl.setBackButtonText('Ma liste');
  }

  //On affiche la page d'édition d'un item
  EditItem() {
    let EditModal = this.modalCtrl.create(EditItemPage,{ 
    title: this.item.title, 
    quantity: this.item.quantity, 
    category: this.item.category,
     image: this.item.image, 
     brand: this.item.brand, 
     weight: this.item.weight,
     sellingCountry: this.item.sellingCountry,
     ingredient: this.item.ingredient,
     packaging: this.item.packaging,
    });
    EditModal.onDidDismiss((item: any) => {
      if (item) {
        this.item.title = item.title;
        this.item.quantity = item.quantity;
        this.item.category = item.category;
        this.item.image = item.image;
        this.item.brand =  item.brand;
        this.item.weight = item.weight;
        this.item.sellingCountry = item.sellingCountry;
        this.item.ingredient = item.ingredient;
        this.item.packaging = item.packaging;
      }
    });
    EditModal.present();
  }

  // garde le bon chemin de fichier
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  //swipe de retour en arriere
  swipeEvent(e) {
    this.navCtrl.popToRoot()
  }
}
