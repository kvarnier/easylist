export { _DetailPage } from './_DetailPage';
export { _MasterPage } from './_MasterPage';
export { DetailItemPage } from './detail-item/detail-item';
export { ListPage } from './list/list';
export { PlaceholderPage } from './placeholder/placeholder';
