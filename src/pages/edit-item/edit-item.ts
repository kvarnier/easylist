import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ListProvider } from '../../providers/list/list';



@IonicPage()
@Component({
  selector: 'page-edit-item',
  templateUrl: 'edit-item.html',
})
export class EditItemPage {

  public item: any = [];
  private inputItem: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public view: ViewController, private formBuilder: FormBuilder, public ListService: ListProvider) {
    this.getData();

    this.inputItem = this.formBuilder.group({
      title: ['', Validators.required],
      quantity: ['', Validators.compose([Validators.maxLength(3), Validators.required])],
      category: [''],
      brand: [''],
      weight: [''],
      sellingCountry: [''],
      ingredient: [''],
      packaging: [''],
    });
  }

  //On récupère la liste dans le stockage
  getData() {
    this.ListService.getListData().then((data) => {
      if (data) {
        this.ListService.itemList = data;
      }
    });
  }

  ionViewDidLoad() {
    this.item.title = this.navParams.get('title');
    this.item.quantity = this.navParams.get('quantity');
    this.item.category = this.navParams.get('category');
    this.item.brand = this.navParams.get('brand');
    this.item.weight = this.navParams.get('weight');
    this.item.sellingCountry = this.navParams.get('sellingCountry');
    this.item.ingredient = this.navParams.get('ingredient');
    this.item.packaging = this.navParams.get('packaging');
  }

  Close(itemParam) {
    this.view.dismiss(itemParam);
  }

  changeItem() {
    let itemParam: any = [];
    for (let item of this.ListService.itemList) {
      if (item.title == this.item.title && item.quantity == this.item.quantity && item.category == this.item.category) {
        if (this.inputItem.value.title != "") {
          item.title = this.inputItem.value.title;
          itemParam.title = this.inputItem.value.title;
        } else itemParam.title = this.item.title;

        if (this.inputItem.value.quantity != "") {
          item.quantity = this.inputItem.value.quantity;
          itemParam.quantity = this.inputItem.value.quantity;
        } else itemParam.quantity = this.item.quantity;

        if (this.inputItem.value.category != "") {
          item.category = this.inputItem.value.category;
          itemParam.category = this.inputItem.value.category;
        } else itemParam.category = this.item.category;

        if (this.inputItem.value.brand != "") {
          item.brand = this.inputItem.value.brand;
          itemParam.brand = this.inputItem.value.brand;
        } else itemParam.brand = this.item.brand;

        if (this.inputItem.value.weight != "") {
          item.weight = this.inputItem.value.weight;
          itemParam.weight = this.inputItem.value.weight;
        } else itemParam.weight = this.item.weight;

        if (this.inputItem.value.sellingCountry != "") {
          item.sellingCountry = this.inputItem.value.sellingCountry;
          itemParam.sellingCountry = this.inputItem.value.sellingCountry;
        } else itemParam.sellingCountry = this.item.sellingCountry;

        if (this.inputItem.value.ingredient != "") {
          item.ingredient = this.inputItem.value.ingredient;
          itemParam.ingredient = this.inputItem.value.ingredient;
        } else itemParam.ingredient = this.item.ingredient;

        if (this.inputItem.value.packaging != "") {
          item.packaging = this.inputItem.value.packaging;
          itemParam.packaging = this.inputItem.value.packaging;
        } else itemParam.packaging = this.item.packaging;
      }
    }
    this.saveItem();
    this.Close(itemParam);
  }

  saveItem() {
    this.ListService.saveListData(this.ListService.itemList);
  }
  
}
