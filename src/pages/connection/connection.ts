import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform, AlertController } from 'ionic-angular';
import { TouchID } from '@ionic-native/touch-id';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth';


@IonicPage()
@Component({
  selector: 'page-connection',
  templateUrl: 'connection.html',
})
export class ConnectionPage {

  public Name: String;
  public Password: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public touchId: TouchID, private platform: Platform, private alertCtrl: AlertController, private androidFingerprintAuth: AndroidFingerprintAuth) {
  }
  //Lorsque on clique sur le bouton de l'empreinte digitale
  verify(){
    //Touch ID pour IOS
    if(this.platform.is('ios') && this.platform.is('cordova')) {
      this.touchId.verifyFingerprint('Scanner votre doigt pour déverrouiller l\'application').then((res) => {
          this.viewCtrl.dismiss();
      }, (err) => {
      });
      //Fingerprint pour android
    }else if (this.platform.is('android') && this.platform.is('cordova')){
      this.fingerPrintAndroid();
      //Lorsque on est pas sur cordova
    }else this.errorAlert("Cet appareil ne possède pas de capteur d\'empreinte");
  }

  //Afficher un message sous forme de popup
  errorAlert(message){
    let alert = this.alertCtrl.create({
      title: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  //Quand on appuis sur le bouton connexion
  connection(){
    this.viewCtrl.dismiss();
  }
  //Implémentation de l'authentification fingerprint pour android
  fingerPrintAndroid(){
    this.androidFingerprintAuth.isAvailable()
  .then((result)=> {
    if(result.isAvailable){
      this.androidFingerprintAuth.encrypt({ clientId: 'easylist' })
        .then(result => {
           if (result.withFingerprint) {
            this.connection();
           }else if (result.withBackup) {
              this.connection();
          }else this.errorAlert("Erreur d'authentification");
        })
        .catch(error => {
          //authentification annulée
        });

    } else {
      this.errorAlert("Capeur d'empreinte non disponible !");
    }
  })
  .catch(error => this.errorAlert("Capeur d'empreinte non disponible !"));
   }

}
