import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, ViewController } from 'ionic-angular';
import { ListProvider } from '../../providers/list/list';
import { NetworkStatusProvider } from '../../providers/network-status/network-status';
import { ListPage } from '../list/list';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { HTTP } from '@ionic-native/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';


declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html',
  providers: [ListPage]
})
export class AddItemPage {

  private inputItem: FormGroup;

  lastImage: string = null;

  itemData = {
    title: "",
    quantity: "",
    category: "",
    image: "",
    brand: "",
    weight: "",
    sellingCountry: "",
    ingredient: "",
    packaging: "",
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public ListService: ListProvider, public ListPage: ListPage, public toastCtrl: ToastController, private formBuilder: FormBuilder, private camera: Camera, private file: File, public platform: Platform, public view: ViewController, private http: HTTP, private barcodeScanner: BarcodeScanner, public NetworkStatus : NetworkStatusProvider) {
    this.inputItem = this.formBuilder.group({
      title: ['', Validators.required],
      quantity: ['1', Validators.compose([Validators.maxLength(3), Validators.required])],
      category: [''],
      image: [''],
    });

  }

  addItemToList() {
    this.saveItem(this.inputItem.value.title, this.inputItem.value.quantity, this.inputItem.value.category, this.inputItem.value.image, this.itemData.brand, this.itemData.weight, this.itemData.sellingCountry, this.itemData.ingredient, this.itemData.packaging, this.itemData.image)
    this.presentToast("Le produit " + this.inputItem.value.title + " a été  ajouté")
    this.Close();
    this.lastImage = null;
    this.ListService.saveListData(this.ListService.itemList);
  }

  saveItem(title, quantity, category, image, brand, weight, sellingCountry, ingredient, packaging, imageScan) {
    let already = false;
    //Gestion des doublons
    for (let item of this.ListService.itemList) {
      if (item.title == title && item.category == category) {
        let itemQuant = parseInt(item.quantity, 10);
        let paramQuant = parseInt(quantity, 10);
        item.quantity = itemQuant + paramQuant;
        already = true;
      }
    }
    if (!already) this.ListService.itemList.push({ title: title, quantity: quantity, category: category, image: image, brand: brand, weight: weight, sellingCountry: sellingCountry, ingredient: ingredient, packaging: packaging, imageScan: imageScan });
  }

  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500,
      position: "botom",
      dismissOnPageChange: false,
      showCloseButton: true,
      closeButtonText: "Fermer",
    });
    toast.present();
  };

  public takePicture() {
    // Options caméra
    var options = {
      quality: 75,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Récupération des données de l'image
    this.camera.getPicture(options).then((imagePath) => {
      var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }, (err) => {
      this.presentToastBis('Erreur de selection.');
    });
  }

  // Creation d'un nouveau nom pour l'image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  //Cope de l'image dans le dossier locale
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.inputItem.value.image = newFileName;
      this.lastImage = newFileName;
    }, error => {
      this.presentToastBis('Erreur de stockage.');
    });
  }

  private presentToastBis(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  Close() {
    this.view.dismiss();
  }

  takeScan() {
    if(!(this.NetworkStatus.Type == "none")){
      this.barcodeScanner.scan(
        {
  
          prompt: "Placez le code barre dans la zone de scan",
        }
  
      ).then(barcodeData => {
        this.getApiData(barcodeData.text)
      }).catch(err => {
        console.log('Error', err);
      });
    }else{
      this.presentToast('Aucune connexion !');
    } 
  }

  getApiData(barre) {
    var url = "https://world.openfoodfacts.org/api/v0/produit/" + barre + ".json"
    this.http.get(url, {}, {})
      .then(data => {
        var itemResultApi = JSON.parse(data.data);
        this.itemData.title = itemResultApi.product.product_name
        this.itemData.category = itemResultApi.product.categories
        this.itemData.image = itemResultApi.product.image_url
        this.itemData.brand = itemResultApi.product.brands
        this.itemData.weight = itemResultApi.product.quantity
        this.itemData.sellingCountry = itemResultApi.product.countries
        this.itemData.ingredient = itemResultApi.product.ingredients_text
        this.itemData.packaging = itemResultApi.product.packaging

        this.presentToast('Produit trouvé');
      })
      .catch(error => {
        this.presentToast('Produit non trouvé');
      });
  }
}

