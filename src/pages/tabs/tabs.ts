import { Component } from '@angular/core';
import { ListPage } from '../list/list';
import { ModalController } from 'ionic-angular';
import { AddItemPage } from '../add-item/add-item';

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {

  tab2Root = ListPage;
  tab3Root = this.presentModal();
  loadPage = false;

  constructor(public modalCtrl: ModalController) {

  }

  ionViewDidLoad() {
    this.loadPage = true;
  }

  presentModal() {
    if (this.loadPage) {
      let AddModal = this.modalCtrl.create(AddItemPage, );
      AddModal.present();
    }
  }
}
