import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';


@Injectable()
export class ListProvider {

  itemList: any = [];
  constructor(public storage: Storage) {
  }

  getListData() {
    return this.storage.get('list');
  }

  saveListData(data) {
    this.storage.set('list', data);
  }

}