import { Injectable } from '@angular/core';
import { Nav } from 'ionic-angular';
import { DetailItemPage } from '../../pages/detail-item/detail-item';
import { PlaceholderPage } from '../../pages/placeholder/placeholder';

@Injectable()
export class NavProxyProvider {

    _masterNav: Nav = null;
    get masterNav(): Nav {
        return this._masterNav;
    }
    set masterNav(value: Nav) {
        this._masterNav = value;
    }
    _detailNav: Nav = null;
    get detailNav(): Nav {
        return this._detailNav;
    }
    set detailNav(value: Nav) {
        this._detailNav = value;
    }
    _isOn: boolean = false;
    get isOn(): boolean {
        return this._isOn;
    }
    set isOn(value: boolean) {
        this._isOn = value;
    }
    pushDetail(page: any, params: any) {
        (this.isOn) ?
            this.detailNav.setRoot(page, params) :
            this.masterNav.push(page, params);
    }
    pushMaster(page: any, params: any) {
        this.masterNav.push(page, params);
    }
    onSplitPaneChanged(isOn) {
        this.isOn = isOn;
        if (this.masterNav && this.detailNav) {
            (isOn) ? this.activateSplitView() :
                this.deactivateSplitView();
        }
    }
    activateSplitView() {
        let currentView = this.masterNav.getActive();
        if (currentView.component.prototype
            instanceof DetailItemPage) {
            this.masterNav.pop();
            this.detailNav.setRoot(
                currentView.component,
                currentView.data);
        }
    }
    deactivateSplitView() {
        let detailView = this.detailNav.getActive();
        this.detailNav.setRoot(PlaceholderPage);
        if (detailView.component.prototype instanceof DetailItemPage) {
            let index = this.masterNav.getViews().length;
            this.masterNav.insert(index,
                detailView.component,
                detailView.data);
        }
    }
}


