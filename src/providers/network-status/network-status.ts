import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';

@Injectable()
export class NetworkStatusProvider {

  public Type: String;

  constructor(private network: Network) {

    this.Type = this.network.type;

    // Etre prévenu lors d'un changement de connexion
    this.network.onchange().subscribe(() => {
      this.Type = this.network.type;
    });

  }

}


